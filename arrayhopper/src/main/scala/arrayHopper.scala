import Tools._

object arrayHopper {
	def main(args: Array[String]): Unit = {
		//val input = Vector(5, 6, 0, 4, 2, 4, 1, 0, 0, 4)
		//val input = Vector(6, 1, 2, 4, 3, 2, 1, 1, 8, 1)
		val input = Vector(3,3,2,1,1)
		//val input = Vector(0)
		//val input = Vector.empty
		//val input = Vector(1,0,0,0)
		val path = Tools.traverse(input)
		if (path == None) println("failed")
		else println(path.mkString(", ") + ", out")
	}
}