package Tools

object Tools {

	sealed abstract class Position()
	case class PositionInBound(value: Int) extends Position
	case class PositionOutOfBound() extends Position
	case class PositionTrap() extends Position 

	
	def traverse(input: Vector[Int]): Option[List[Int]] = {
		
		def hop(input: Vector[Int], path: List[Int], currentValue: Int): Option[List[Int]] = {

			def getNextPos(subArray: Vector[Int], currentPos: Int): Position = {
				if (subArray.isEmpty) PositionTrap()
				else {
					val hopToPositions = subArray.zipWithIndex.map{case (v, i) => v + i}
					val nextPos = hopToPositions.indexOf(hopToPositions.max) + currentPos
					
					if (nextPos == 0) PositionTrap()
					else if (nextPos > input.length) PositionOutOfBound()
					else if (input(nextPos) == 0) getNextPos(subArray.filter(_ != subArray.max), currentValue)
					else {
						val possibleFromPoss = hopToPositions.zipWithIndex.filter{case (v, i) => v == hopToPositions.max}.map(_._2)
						PositionInBound(nextPos)	
					}
				}
			}
			
			if (input.size < path.last + currentValue + 1) {
				val newPos = path.last + currentValue + 1
				Some(path)
			}
			else {
				val subArray = input.slice(path.last, path.last + currentValue + 1)
				val newPos = getNextPos(subArray, path.last)
			
				newPos match {
					case PositionOutOfBound() => Some(path.dropRight(1))
					case PositionInBound(v) => hop(input, path:+v, input(v))
					case PositionTrap() => None
				}	
			}
		}

		if (input.isEmpty) Some(List.empty)
		else hop(input, List(0), input.head)
	}
}
