#!/usr/bin/env python

def jump(nums, pos):
	pos.append(0)
	start = end = step = 0
	while end < len(nums):
		start, end = end+1, max([i+nums[i] for i in range(start, end+1)])
		pos.append(start)
		step += 1
	pos = pos[:-1]
	return step, pos

numbers = [3,3,2,1,1]
positions = []

print(jump(numbers, positions))